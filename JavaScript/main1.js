function personFactory() { 
    var details = {
        firstName: 'Zosia',
        lastName: 'Kraweznik',
        accountsList: [
            {
                balance: 500,
                currency: 'NZD'
            }
        ]
    }


    return { 
        firstName: details.firstName, 
        lastName: details.lastName,
        sayHello: function () { 
            return this.firstName + ' ' + this.lastName + ' ' + details.accountsList.length;
        }
    }

}
    //create an object using personFactory function and assign it to a variable
   var person = personFactory(); 
   console.log( person.sayHello() );



