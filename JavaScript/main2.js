var person = (function () { //local method, return bedzie module obcject
    var details = {
        firstName: 'Zosia',
        lastName: 'Kraweznik',
    };

    function calculateBalance() {//add local calculateBalance() method
        var balanceSum = 0;
        for (var i = 0; i < this.accountsList.length; i++) {
            var account = this.accountsList[i];
            balanceSum += account.balance;
        }

        // for (var i in this.accountsList) {
        //     var account = this.accountsList[i];
        //     balanceSum += account.balance;
        // }

        // for (var account of this.accountsList) {
        //     balanceSum += account.balance;
        // }

        // this.accountsList.forEach(function(account){
        //     balanceSum += account.balance;
        // });

        return balanceSum;
    }


    return { // personFactory should return object with properties firstName, lastName and method sayHello
        firstName: details.firstName, //firstName and lastName should take its values from inner details object
        lastName: details.lastName,
        accountsList: [
            {
                balance: 500,
                currency: 'NZD'
            },
            {
                balance: 300,
                currency: 'NZD'
            }
        ],
        addAccount: function (account) {
            this.accountsList.push(account); // push method  to add element to list
        },                                   
        
        sayHello: function () { 
            return this.firstName + ' ' + this.lastName + ' ' + this.accountsList.length + ' ' + calculateBalance.call(this);
        }
        
    }

})(); // tu kończy się IIFE z zewnątrz nie można modyfikować rzeczy w details 

 
var someAccount = {
    balance: 3000,
    currency: 'NZD'
};
console.log(person.sayHello());
person.addAccount(someAccount);
console.log(person.sayHello());



