class Person {
    constructor(firstName, lastName, accounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    };

    _calculateBalance() {
        var balanceSum = 0;
        for (var i = 0; i < this.accounts.length; i++) {
            var account = this.accounts[i];
            balanceSum += account.balance;
        }
        return balanceSum;
    }

    sayHallo() {
        return this.firstName + ' ' + this.lastName + ' ' + this.accounts.length + ' ' + calculateBalance.call(this);
    }

    addAccount(account) {
        this.accounts.push(account);
    }

    filterPositiveAccounts() {
        return this.accounts.filter(account => account.balance > 0);
    }

    findAccount(accountNumber){
        return this.accounts.find(function (account) { return account.number === accountNumber });
    }

    withdraw(accountNumber, amount){
        let thisPersonsAccount = this.findAccount(accountNumber); // pod returna a nad ifa lepiej
        return new Promise((resolve, reject) => {
            if (amount <= 0) {
                reject("Amount must be positive.");
            } else if (thisPersonsAccount != undefined && amount <= (thisPersonsAccount.balance)) {
                thisPersonsAccount.balance -= amount;
                setTimeout(() => (resolve(`Account ${accountNumber} new balance: ${thisPersonsAccount.balance} ${thisPersonsAccount.currency}, after withdrawal of ${amount} ${thisPersonsAccount.currency}`)), 3000);
            } else {
                reject("The account number does not exist or the amount to withdraw is too big");
            }
        })
    }
};

class Account {
    constructor(balance, currency, number) {
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
};