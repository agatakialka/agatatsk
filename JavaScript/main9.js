//add a new js file and create a new person with 2 acocunts
(function () {
    const account1 = new Account(5000, "denars", 1);
    const account2 = new Account(10000, "denars", 2);
    //const account3 = new Account(10000, "denars", 3); // w zadaniu było 2 accounts

    const person = new Person("Geralt", "Z Rivii", [account1, account2]);

    //in the created js file write code for displaying person with accounts in the created card

    //document to jest taki globalny obiekt, główny węzeł drzewa DOM
    // zmienna nameElement do której jest pobrany element html o id name
    const nameElement = document.querySelector('#name');

    //na prezentacji jest .innerHTML ale może to być niebezpieczne, więc uzywam .textContent
    nameElement.textContent = `${person.firstName} ${person.lastName}`;

    //chce wyswietlic accountsy a jest ich wiele (czyli 2) stad petla
    //w html napisane jest ze trzeba stworzyc paragrafy to tworzymy dla kazdego elemtu listy accountsList

    function showAccounts() { //opakowane w funkcje bo potrzebne wiecej niz 1
        const accountList = document.querySelector('#accounts');
        accountList.innerHTML = ''; //zeby po wypisaniu nowych wartosci po withdraw wyczyscil stare wartosci
        for (let i = 0; i < person.accounts.length; i++) {
            let account = person.accounts[i];
            let paragraph = document.createElement('p');
            paragraph.textContent = `${account.number}. ${account.balance} ${account.currency}`;
            accountList.appendChild(paragraph);
        };
    }

    showAccounts(); //po wczytaniu strony zeby sie wyswietlila lista


    /*
    to samo co wyzej tylko latwiej
    person.accountsList.forEach(function(account){
    
    });
    
    for(let account of person.accountsList) {
    
    }
    */

    //wybierm elementy z htmla:
    const button = document.querySelector('button.btn');

    function withdraw() {
        //write a method which will read acocunt number, amount and call person.withdraw: wartość w elemencie pole.value
        const accountNumberEl = document.querySelector('#number');
        const withdrawAmountEl = document.querySelector('#amount');

        const accountNumber = Number(accountNumberEl.value);
        const withdrawAmount = Number(withdrawAmountEl.value);
        
        person.withdraw(accountNumber, withdrawAmount).then(function (successMessage) {
            console.log(successMessage);
            const infoMessage = document.querySelector('#info');
            infoMessage.textContent = successMessage;
            //showMessage(successMessage);
            showAccounts(); //zeby sie lista zupdatowala jesli sie zrobi withdraw
        })
            .catch(function (failMessage) { 
                console.log(failMessage)
                const infoMessageTwo = document.querySelector('#info');
                infoMessageTwo.textContent = failMessage;
                //showMessage(successMessage);
            });

    }

    // function showMessage(message) { USPRAWNIENIE w withdraw mozna odwolywac sie do tej funkcji zamist 2 razy pisać to samo
    //     const infoMessage = document.querySelector('#info');
    //     infoMessage.textContent = message;
    // }

    function clearMessage() {
        const infoMessageTwo = document.querySelector('#info');
        infoMessageTwo.textContent = '';
    }

    //reacting on events:

    button.addEventListener('click', function () {
        clearMessage();
        withdraw();
    });

})(); // (function () { })() --> 5.wrap the code in the module in order not to polute global scope
