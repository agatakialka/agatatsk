var person = (function () { //local method, return bedzie module obcject
    var details = {
        firstName: 'Zosia',
        lastName: 'Kraweznik',
    };

    function calculateBalance() {//add local calculateBalance() method
        var balanceSum = 0;
        for (var i = 0; i < this.accountsList.length; i++) {
            var account = this.accountsList[i];
            balanceSum += account.balance;
        }

        // for (var i in this.accountsList) {
        //     var account = this.accountsList[i];
        //     balanceSum += account.balance;
        // }

        // for (var account of this.accountsList) {
        //     balanceSum += account.balance;
        // }

        // this.accountsList.forEach(function(account){
        //     balanceSum += account.balance;
        // });

        return balanceSum;
    }


    return { // personFactory should return object with properties firstName, lastName and method sayHello
        firstName: details.firstName, //firstName and lastName should take its values from inner details object
        lastName: details.lastName,
        accountsList: [ // UZYC FUNKCJI KONSTRUKTOROWEJ
            {
                balance: 500,
                currency: 'NZD'
            },
            {
                balance: 300,
                currency: 'NZD'
            }
        ],
        addAccount: function (account) {//klamerki zawsze jak sie deklaruje metodę, tam jest ciało funcji
            this.accountsList.push(account); // push method  to add element to list
        },                                   // balance i currency wypisane na dole w zmiennej Account

        sayHello: function () { //sayHello method should return a string with firstName, lastName and number of accounts assigned to an object created by personFactory
            return this.firstName + ' ' + this.lastName + ' ' + this.accountsList.length + ' ' + calculateBalance.call(this);
        }
        //showBalance: calculateBalance
        // albo calculateBalance z call i this a bez showBalance
        
    }

})();

var Account = function (balance, currency) { //Account constructor function
    this.balance = balance;
    this.currency = currency;
};

var someAccount = new Account(200, 'NZD'); // constructor function call, przy pomocy konstruktora tworze nowy obiekt
console.log(person.sayHello());
person.addAccount(someAccount); //albo tu w nawiasie można napisać(new Account (200, 'USD') )
// person.addAccount(new Account(500, 'PLN') );
console.log(person.sayHello());







