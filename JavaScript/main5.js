class Person {
    constructor(firstName, lastName, accounts) { // constructor of the Person class should take firstName, lastNames and accounts list as parameters
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    };

    _calculateBalance() { //class method _calculateBalance()
        var balanceSum = 0;
        for (var account of this.accountsList) { 
            balanceSum += account.balance;
        }
        return balanceSum;
    }

    sayHallo() {
        return this.firstName + ' ' + this.lastName + ' ' + this.accounts.length + ' ' + calculateBalance.call(this);
    }

    addAccount(account) {
        this.accounts.push(account); //dodaje account do listy
    }

    filterPositiveAccounts() {
        return this.accounts.filter(account => account.balance > 0);
    }

    findAccount(accountNumber) { // zwróci pierwszy element tablicy z accounts, który spełnia warunek podanej funkcji testującej
        return this.accounts.find(function (account) {
            return account.number === accountNumber
        });
    }

    withdraw(accountNumber, amount) {

        return new Promise((resolve, reject) => {
            let thisPersonsAccount = this.findAccount(accountNumber); // przeniesione z 35 bo gdyby znajdowanie Account długo trwało to bedzie odłożone w czasie i nie rozwali programu
            if (thisPersonsAccount != undefined && amount < (thisPersonsAccount.balance)) { // jeśli account istnieje i amount jest większa niż balans to RESOLVE
                thisPersonsAccount.balance -= amount; //musi sie zmienic
                setTimeout(() => (resolve(`Account ${accountNumber} new balance: ${thisPersonsAccount.balance}, after withdrawal of ${amount}`)), 3000);
            } else {
                reject("The account number does not exist or the amount to withdraw is too big");
            }
        })
    }
};

class Account {
    constructor(balance, currency, number) {
        this.balance = balance;
        this.currency = currency;
        this.number = number; // add number property to Account class
    }
}



account1 = new Account(5000, "denars", 1);
account2 = new Account(10000, "denars", 2);
account3 = new Account(-1000, "denars", 3);
account4 = new Account(7000, "denars", 4);

const person1 = new Person("Zosia", "Kraweznik", [account1, account2]);

person1.addAccount(account3);
person1.addAccount(account4);

person1.withdraw(3,10)  //person.withdraw(accNumber, amount).then(...).catch(...)
    .then(function (successMessage) { //wraps a value in a promise and resolves it
        console.log(successMessage);
    })
    .catch(function (failMessage) { //wraps a value in a promise and rejects it
        console.log(failMessage)
    });
person1.withdraw(4, 20000)
    .then(function (successMessage) {
        console.log(successMessage);
    })
    .catch(function (failMessage) {
        console.log(failMessage);
    });


// findAccount(accountNumber) { // zwróci pierwszy element tablicy z accounts, który spełnia warunek podanej funkcji testującej
//     return this.accounts.find(function (account) {
//         return account.number === accountNumber
//     });
// }

// findAccount(accountNumber) {

//     return new Promise((resolve, reject) => {
//         if (this.accounts.find(function (account) {
//             account.number === accountNumber
//         })) {
//             setTimeout(() => (resolve(`resolve info`)), 3000);
//         } else {
//             reject("reject info");
//         }
//     })
// }

