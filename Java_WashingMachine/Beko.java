
public class Beko extends WashingMachine {
	
	public Beko(int program, double temp, int spin) {
		super(program, temp, spin, BrandName.Beko);
		this.tempStep = 1.0;
	}
	
	public Beko() {
		super();
		this.brandName = BrandName.Beko;
		this.tempStep = 1.0;
	}
	
}
