
public class Whirpool extends WashingMachine {
	
	public Whirpool(int program, double temp, int spin) {
		super(program, temp, spin, BrandName.Whirpool);
		this.programMax = 25;
	}
	
	public Whirpool() {
		super();
		this.brandName = BrandName.Whirpool;
		this.programMax = 25;
	}
	
}
