public abstract class WashingMachine {
	
	protected int	programMin	= 1;
	protected int	programMax	= 20;
	protected int	program		= programMin;
	
	private double		tempMin		= 0.0;
	private double		tempMax		= 90.0;
	protected double	tempStep	= 0.5;
	protected double	temp		= tempMin;
	
	private int	spinMin		= 0;
	private int	spinMax		= 1000;
	private int	spinStep	= 100;
	private int	spin		= spinMin;
	
	protected BrandName brandName;
	
	public WashingMachine() {
		this.program = 1;
		this.temp = tempMin;
		this.spin = spinMin;
		
	}
	
	public WashingMachine(int program, double temp, int spin, BrandName brand) {
		this.program = program;
		this.temp = temp;
		this.spin = spin;
		this.brandName = brand;
	}
	
	public int getProgram() {
		return this.program;
	}
	
	public void setProgram(int program) throws RuntimeException {
		if (program <= programMax && program >= programMin) {
			this.program = program;
		} else {
			throw new RuntimeException("Please set program between " +
					this.programMin + " and " + this.programMax + ".");
		}
	}
	
	public void nextProgram() {
		if (program + 1 <= programMax) {
			this.setProgram(program + 1);
		} else {
			program = programMin;
		}
	}
	
	public void previousProgram() {
		if (program - 1 >= programMin) {
			this.setProgram(program - 1);
		} else {
			program = programMax;
		}
	}
	
	//
	
	public double getTemp() {
		return this.temp;
	}
	
	public boolean validateTemp(double temp) {
		if (temp <= tempMax && temp >= tempMin) {
			return true;
		}
		System.out.println("Temperature " + temp + "\u00b0C is out of range.");
		return false;
		
	}
	
	public void setTemp(double temp) throws RuntimeException {
		if (validateTemp(temp)) {
			this.temp = ((double) (Math.round(temp * 2)) / 2);
			System.out.println("Current temperature is " + this.temp + "\u00b0C");
		} else
			throw new RuntimeException("Please set temperature between " +
					this.tempMin + " and " + this.tempMax + ".");
	}
	
	public void tempUp() {
		setTemp(temp + tempStep);
	}
	
	public void tempDown() {
		setTemp(temp - tempStep);
	}
	
	//
	
	public int getSpin() {
		return spin;
	}
	
	public void setSpin(int spin) throws RuntimeException {
		if (spin >= spinMin && spin <= spinMax) {
			this.spin = spin;
		} else
			throw new RuntimeException("Please set spin between " +
					this.spinMin + " and " + this.spinMax + ".");
	}
	
	public void spinUp() {
		if (spin + spinStep <= spinMax) {
			spin += spinStep;
		} else {
			spin = spinMin;
		}
	}
	
	public void spinDown() {
		if (spin - spinStep >= spinMin) {
			spin -= spinStep;
		} else {
			spin = spinMax;
		}
		
	}
	
	public BrandName getBrandName() {
		return this.brandName;
	}
	
	public void showStatus() {
		System.out.println("Brand: " + getBrandName());
		System.out.println("Program number: " + getProgram());
		System.out.println("Temperature: " + getTemp() + "\u00b0C");
		System.out.println("Spin: " + getSpin());
		System.out.println("__________________________");
	}
	
}
