
public class Amica extends WashingMachine {
	
	public Amica(int program, double temp, int spin) {
		super(program, temp, spin, BrandName.Amica);
	}
	
	public Amica() {
		super();
		this.brandName = BrandName.Amica;
	}
	
}
