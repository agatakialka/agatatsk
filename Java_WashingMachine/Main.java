import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		
		System.out.println("AMICA");
		Amica pralkaAmica = new Amica();
		
		System.out.println("BEKO");
		Beko pralkaBeko = new Beko();
		
		System.out.println("WHIRPOOL");
		Whirpool pralkaWhirpool = new Whirpool();
		
		System.out.println();
		System.out.println("SORTING");
		
		List<WashingMachine> allWashingMachines = new ArrayList<WashingMachine>();
		Amica amica = new Amica();
		Beko beko = new Beko();
		Whirpool whirpool = new Whirpool();
		
		allWashingMachines.add(whirpool);
		allWashingMachines.add(beko);
		allWashingMachines.add(amica);
		
		// allWashingMachines.stream()
		// .forEach(WashingMachine::upTemp);
		
		// allWashingMachines.stream()
		// .forEach(WashingMachine::showStatus);
		
		System.out.println("List before sorting: ");
		allWashingMachines.stream()
				.forEach(p -> System.out.println("# " + p.getBrandName()));
		// .forEach(p -> p.showStatus());
		
		Collections.sort(allWashingMachines, new Comparator<WashingMachine>() {
			@Override
			public int compare(WashingMachine w1, WashingMachine w2) {
				return w1.getBrandName()
						.toString()
						.compareTo(w2.getBrandName()
								.toString());
			}
		});
		System.out.println();
		System.out.println("List after sorting: ");
		allWashingMachines.stream()
				// .forEach(p -> p.showStatus());
				.forEach(p -> System.out.println("- " + p.getBrandName()));
	}
}
